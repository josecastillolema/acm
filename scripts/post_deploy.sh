#!/bin/bash

## This script is executed after the CODECO operator deployment finishes. It is used to install additional 
## components and to perform any other post-deployment tasks.

echo "Executing post deployment tasks..."

##TODO(user): Add your post deployment tasks here
